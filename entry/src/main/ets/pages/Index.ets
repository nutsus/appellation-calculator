/*
 * Copyright (c) 2021 JianGuo Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 该组件为主界面。
 * 包含：称谓。关系，两者，合称 四个tab页面

 */
import { BothTabContent } from '../tabcontent/BothTabContent';
import { BottomTabs } from '../common/bottomTabs';
import { relationTabContent } from '../tabcontent/relationTabContent';
import { collectiveNameTabContent } from '../tabcontent/collectiveNameTabContent';
import { appellationTabContent } from '../tabcontent/appellationTabContent';
import { CustomDialogPrivacy } from '../dialog/CustomDialogPrivacy';
import { PreferencesUtil } from '../utils/PreferencesUtil';

let preferencesUtil = new PreferencesUtil();

@Entry
@Component
struct Index {
  private controller: TabsController = new TabsController()
  @State bottomTabIndex: number = 0
  @State fontColor: string = '#AAAAAA'
  @State selectedFontColor: string = '#E7763E'
  @State currentIndex: number = 0
  // 隐私协议标题
  private title?: string = '协议和隐私政策提示'
  // 前辍隐私协议信息
  private prefixMessage?: string = '感谢您选择亲戚关系计算器元服务！我们非常重视您的个人信息和隐私保护。为更好地保障您的个人权益，在使用产品前，请您认真阅读'
  // 后辍隐私协议信息
  private suffixMessage?: string = '的全部内容。'
  // 隐私协议信息，点击可以跳转
  private privacyMessage?: string = '《协议与隐私政策》'
  // 本地html文件或http和https返回html文件
  private localHtml?: boolean = true
  // 隐私协议URL 支持本地html或http和https返回html
  private urlPage?: string = ""
  // 开始显示隐私协议对话框
  /**
   * 如果localHtml参数为true,urlPage参数为空，显示默认隐私协议
   * 如果localHtml参数为true,urlPage参数不为空，显示urlPage参数本地html文件
   * 如果localHtml参数为false,urlPage参数为空，显示默认隐私协议
   * 如果localHtml参数为false,urlPage参数不为空，显示urlPage参数http或https返回html文件
   */
  privacyDialogController: CustomDialogController = new CustomDialogController({
    builder: CustomDialogPrivacy({
      localHtml: false,
      urlPage: 'https://static-mp-ee26bc12-484e-4d4b-a72f-6f9d41069ff6.next.bspapp.com/agreement/qqgxjsq.html',
      prefixMessage: this.prefixMessage
      // urlPage: 'https://ost.51cto.com/posts/25197'
    }),
    autoCancel: false,
    alignment: DialogAlignment.Center,
    customStyle: true
  })

  onPageShow() {
    console.info('xx onPageShow 显示隐私协议')
    preferencesUtil.getChangePrivacy().then((value) => {
      console.info(`xx onPageShow 获取隐私协议状态：${value}`)
      if (!value) {
        this.privacyDialogController.open()
      }
    })

  }

  @Builder
  TabBuilder(index: number, name: string, icon: Resource, activeIcon: Resource) {
    Column() {
      Image(this.currentIndex === index ? activeIcon : icon)
        .width(28)
      Text(name)
        .fontColor(this.currentIndex === index ? this.selectedFontColor : this.fontColor)
        .fontSize(16)
        .fontWeight(this.currentIndex === index ? 500 : 400)
        .lineHeight(22)
    }.width('100%')
  }

  build() {
    Column() {
      Tabs({ barPosition: BarPosition.End, controller: this.controller }) {
        TabContent() {
          appellationTabContent()
        }.tabBar(this.TabBuilder(0, '称谓', $r('app.media.tabOne'), $r('app.media.tabOneActive')))

        TabContent() {
          relationTabContent()
        }.tabBar(this.TabBuilder(1, '关系', $r('app.media.tabTwo'), $r('app.media.tabTwoActive')))

        TabContent() {
          BothTabContent()
        }.tabBar(this.TabBuilder(2, '两者', $r('app.media.tabThree'), $r('app.media.tabThreeActive')))

        TabContent() {
          collectiveNameTabContent()
        }.tabBar(this.TabBuilder(3, '合称', $r('app.media.tabFour'), $r('app.media.tabFourActive')))
      }
      .vertical(false)
      .barMode(BarMode.Fixed)
      .barWidth(360)
      .barHeight(56)
      .animationDuration(400)
      .onChange((index: number) => {
        this.currentIndex = index
      })
      .backgroundColor('#444444')
    }.width('100%')
  }
}