/*
 * Copyright (c) 2021 JianGuo Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 该组件为底部标签栏，主界面使用。
 * 导航栏固定高度78vp，宽度为100%

 */


/**
 * 底部标签页每个项数据
 * 这段数据是固定不变的，无需暴露model接口，就放在该文件中
 * @param imgSrcNomral 按钮图片
 * @param imgSrcPress 按下后图片
 * @param tabText 按钮文字
 */
class TabItem {
  imgSrcNomral: Resource
  tabText: string

  constructor(imgSrcNomral: Resource, tabText: string) {
    this.imgSrcNomral = imgSrcNomral

    this.tabText = tabText
  }
}

/**
 * 用于初始化TabItem的数组
 */
function getTabItems(): Array<TabItem> {
  let itemsArray: Array<TabItem> = []
  itemsArray.push(new TabItem($r('app.media.title'), "称谓"))
  itemsArray.push(new TabItem($r('app.media.relationship'), "关系"))
  itemsArray.push(new TabItem($r('app.media.both'), "两者"))
  itemsArray.push(new TabItem($r('app.media.pray'), "合称"))
  return itemsArray;
}

@Component
export struct BottomTabs {
  private tabSrc: number[] = [0, 1, 2, 3]
  private controller: TabsController = new TabsController()
  @Link bottomTabIndex: number
  private tabItemArray: Array<TabItem> = getTabItems()

  build() {
    Flex({ direction: FlexDirection.Row, alignItems: ItemAlign.Center, justifyContent: FlexAlign.SpaceEvenly }) {
      ForEach(this.tabSrc, item => {
        Column() {
          Image(this.tabItemArray[item].imgSrcNomral)
            .objectFit(ImageFit.Contain).backgroundColor(this.bottomTabIndex == item ? Color.Orange : Color.Black)
            .width(30).height(30)
          Text(this.tabItemArray[item].tabText)
            .fontSize(14)
            .fontFamily($r('sys.float.ohos_id_text_size_body2'))
            .margin({ top: 3 })
        }
        .onClick(() => {
          this.controller.changeIndex(item)
        })
      }, item => item.toString())
    }
    .width('100%').height(74)
    .backgroundColor($r('app.color.grey0'))
  }
}