import relationship from "@nutpi/relationship"
import router from '@ohos.router';

@Component
export struct appellationTabContent {
  @State text: string = ''
  @State sex: number = 1 // 1/男，0/女
  @State sexFlag: number = 1 // 1/男，0/女
  @State result: string = ''
  @State bothflag: boolean = false
  controller: TextInputController = new TextInputController()
  @State panelData: {
    value: string,
    label: string,
    icon: boolean,
    sex?: number,

    [key: string]: any
  }[] = [
    { value: '爸爸', label: '父', icon: false, sex: 1 },
    { value: '妈妈', label: '母', icon: false, sex: 0 },
    { value: '', label: '/svg/leftArrow.svg', icon: true, back: true },
    { value: '', label: '/svg/refresh.svg', icon: true, resetting: true },
    { value: '哥哥', label: '兄', icon: false, sex: 1 },
    { value: '姐姐', label: '姐', icon: false, sex: 0 },
    {
      value: '老公',
      label: '夫',
      icon: false,
      sex: 1,
      isDisable: true
    },
    {
      value: '老婆',
      label: '妻',
      icon: false,
      sex: 0,
      isDisable: true
    },
    { value: '弟弟', label: '弟', icon: false, sex: 1 },
    { value: '妹妹', label: '妹', icon: false, sex: 0 },
    { value: '', label: '/svg/invert.svg', icon: true, invert: true },
    { value: '', label: '=', icon: false, count: true },
    { value: '儿子', label: '子', icon: false, sex: 1 },
    { value: '女儿', label: '女', icon: false, sex: 0 },
    { value: '', label: '?', icon: false, more: true }
  ]

  build() {
    Column() {
      Column() {
        Column() {
          Text(this.bothflag ? (this.sex ? '他称呼我' : '她称呼我') : (this.sex ? '我称呼他' : '我称呼她'))
            .width('100%')
            .fontColor('#BBBBBB')
          Row() {
            Text(this.text)
              .fontColor('#BBBBBB')
              .fontSize('18fp')
          }
          .width('100%')
          .justifyContent(FlexAlign.End)
          .layoutWeight(1)
        }
        .height('55%')

        Row() {
          Text(this.result)
            .fontColor(Color.White)
            .fontSize('22fp')
        }
        .width('100%')
        .justifyContent(FlexAlign.End)
        .height('45%')
      }
      .height('40%')
      .width('100%')
      .padding({ top: 5, left: 15, right: 15, bottom: 5 })
      .backgroundColor('#333333')

      Row() {
        Row() {
          Text('男')
            .fontColor(Color.White)
          Toggle({ type: ToggleType.Switch, isOn: false })
            .selectedColor('#5A3E30')
            .switchPointColor('#FFFFFF')
            .onChange((isOn: boolean) => {
              this.sex = isOn ? 0 : 1
              this.sexFlag = isOn ? 0 : 1
              this.text = ''
              this.result = ''
            })
            .width(50)
            .height('100%')
            .backgroundColor('#5A3E30')
            .borderRadius(20)
          Text('女')
            .fontColor(Color.White)
        }

        Text("关于作者")
          .fontSize('12fp')
          .fontColor('#BBBBBB')
          .onClick(() => {
            router.pushUrl({
              url: "pages/AboutPage"
            })
          })
      }
      .padding({ top: 10, left: 15, right: 15, bottom: 10 })
      .backgroundColor('#2B2B2B')
      .height('7%')
      .width('100%')
      .justifyContent(FlexAlign.SpaceBetween)

      Row() {
        Grid() {
          ForEach(this.panelData, (item) => {
            GridItem() {
              Row() {
                //表示为汉字
                if (!item.icon) {
                  Text(item.label)
                    .fontColor(item.sex == this.sex && item.isDisable ? '#676667' : Color.White)
                } else {
                  Image(item.label)
                    .width(22)
                }
              }
            }
            .border({ width: 1, color: item.count ? '#E66747' : '#434343' })
            .rowStart(0)
            .rowEnd(item.count ? 1 : 0)
            .backgroundColor(item.count ? '#E66747' : (item.invert && this.bothflag ? '#292929' : '#333333'))
            .stateStyles({
              pressed: this.pressedStyle,
            })
            .onClick(() => {
              this.handelClick(item)
            })
          })
        }
        .rowsTemplate('1fr 1fr 1fr 1fr')
        .columnsTemplate('1fr 1fr 1fr 1fr')
      }
      .backgroundColor('#333333')
      .height('53%')
      .width('100%')
    }.width("100%").height("100%").backgroundColor(Color.Orange)

  }

  //宫格按下变暗
  @Styles
  pressedStyle(){
    .backgroundColor('#2B2B2B')
    .border({ width: 1, color: '#2B2B2B' })
  }

  //点击面板
  handelClick(item) {
    //左箭头 表示回退一步
    if (item.back) {
      this.back()
    } else if (item.resetting) { //刷新 表示清除
      this.text = ''
      this.result = ''
    } else if (item.invert) { //倒转 表示改变称呼方向
      this.bothflag = !this.bothflag
      if (!this.bothflag && (this.result.split('的').length == 1)) {
        this.result = this.text
      }
      this.count()
    } else if (item.more) { //？问号 表示了解更多
      router.pushUrl({
        url: "pages/details"
      })
    } else if (item.count) { //= 等于 表示计算称呼
      this.count()
    } else { //正常点击文字
      if (item.sex == this.sex && item.isDisable) { //表示点击的是禁用状态的父妻
        this.count()
      }
      this.add(item.value)
      this.sex = item.sex
    }
  }

  //回退一步
  back() {
    let data = this.result.split('的')
    if (data.length >= 1 && data[0]) {
      data.splice(data.length - 1, 1)
      this.result = data.join(',').replace(/,/, '的')
    } else {
      this.text = ''
    }
  }

  //点击正常文字
  add(item) {
    if (this.result) {
      let index = this.result.indexOf('/')
      if (index != -1) {
        this.result = this.result.slice(0, index) + '的' + item
      } else {
        this.result += '的' + item
      }
    } else {
      this.result += item
    }
  }

  //计算称呼
  async count() {
    try {
      let data = this.result
      let res: any = await relationship({
        text: data,

        sex: this.sexFlag,
        type: 'default',

        reverse: this.bothflag
      });
      this.result = res.join('/')
      //接口请求成功赋值给text
      this.text = data
    } catch (e) {
    }
  }
}